{-# LANGUAGE TemplateHaskell, TypeFamilies, FlexibleInstances, UndecidableInstances #-}
{-# OPTIONS_GHC -ddump-splices #-}
module Overload.Example where

import Data.Maybe
import Overload

f1 :: Bool
f1 = True

f2 :: Int -> Int
f2 x = x + 1

f3 :: Num a => Maybe a
f3 = Just 0

overload "f" ['f1, 'f2, 'f3]

test :: IO ()
test = do
    print (f 1)
    print (f && True)
    print (fromMaybe 10 f)

g1 :: (Int, Char)
g1 = undefined
g2 :: (Bool, ())
g2 = undefined
g3 :: (Int, ())
g3 = undefined

overload "g" ['g1, 'g2, 'g3]

h1 :: Maybe a
h1 = Nothing
h2 :: Maybe Int
h2 = Just 2

-- overload "h" ['h1, 'h2]
